#
### https://daiderd.com/nix-darwin/manual/index.html
#
{
  description = "Darwin system flake for MacBookPro";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs }:
  let
    configuration = { pkgs, ... }: {

      nixpkgs.config = {
        allowBroken = true;
        allowUnsupportedSystem = true;
        permittedInsecurePackages = [
          "python3.12-youtube-dl-2021.12.17"
        ];
      };

      environment.systemPackages = [
          pkgs.google-cloud-sdk-gce
          pkgs.vim
          pkgs.cfssl
          pkgs.certmgr
          pkgs.k9s
          pkgs.kubectl
          pkgs.jq
          pkgs.nmap
          pkgs.php83
          pkgs.php83Packages.composer
          pkgs.transmission_3
          pkgs.tailscale
          pkgs.yq
          pkgs.git
          pkgs.htmlq
          pkgs.vscode-with-extensions
          pkgs.vscode-extensions.tailscale.vscode-tailscale
          pkgs.vscode-extensions.redhat.ansible
          pkgs.terraform
          pkgs.tfswitch
          pkgs.esptool
          pkgs.espflash
          pkgs.terraform-providers.ansible
          pkgs.warp-terminal
          pkgs.ansible
          pkgs.ansible-lint
          pkgs.android-tools
          pkgs.ffmpeg
          pkgs.binwalk
          pkgs.neofetch
          pkgs.mqttui
          #pkgs.ghidra
          pkgs.android-tools
        ];

      ### https://formulae.brew.sh/

      homebrew = {
        enable = true;
        
        brews = [
          {
            name = "vfkit";
            link = true;
            args = [ "HEAD" ];
          }
          "make"
          "go-md2man"
          "mas"
          "tree"
        ];

        taps = [
          "cfergeau/crc"
        ];
        casks = [ 
          "transmission"
          "raycast"
          "gimp"
        ];
        masApps = {
            "1Password for Safari" = 1569813296;
            Xcode = 497799835;
            #"Capital One Shopping Extension" = 1477110326;
            "Grammarly: AI Writing Support" = 1462114288;
            #"Ghostery – Privacy Ad Blocker" = 1436953057;
            #"Aura for Safari" = 6449464918;
            #"Ground News" = 1324203419;
            #"Wayback Machine" = 1472432422;
            "HP Easy Scan" = 967004861;
            "Zeroconf Browser" = 1355001318;
            "Microsoft Remote Desktop" = 1295203466;
            "HP Smart" = 1474276998;
        };
      };

      # Auto upgrade nix package and the daemon service.
      services.nix-daemon.enable = true;
      nix.package = pkgs.nix;

      # Necessary for using flakes on this system.
      nix.settings.experimental-features = "nix-command flakes";
      nixpkgs.config.allowUnfree = true;

      # Create /etc/zshrc that loads the nix-darwin environment.
      programs.zsh.enable = true;  # default shell on catalina
      # programs.fish.enable = true;

      # Set Git commit hash for darwin-version.
      system.configurationRevision = self.rev or self.dirtyRev or null;

      security.pam.enableSudoTouchIdAuth = true;

      # Used for backwards compatibility, please read the changelog before changing.
      # $ darwin-rebuild changelog
      system.stateVersion = 4;

      nix.settings.trusted-users = ["giezac"];

      system.defaults = {
        finder.AppleShowAllExtensions = true;
        screencapture.location = "~/Pictures/screenshots";
        screensaver.askForPasswordDelay = 10;
      };

      #nix.extraOptions = ''
      #  extra-platforms = x86_64-darwin aarch64-darwin
      #'';

      nix.linux-builder.enable = true;

      # The platform the configuration will be used on.
      nixpkgs.hostPlatform = "x86_64-darwin";

      ids.uids.nixbld = 300;

      /*
      boot.binfmt.emulatedSystems = [ 
        "aarch64-linux"
        "armv6l-linux"
        "armv7l-linux"
        "i386-linux"
        "i486-linux"
        "i586-linux"
        "i686-linux"
        "powerpc-linux"
        "powerpc64-linux"
        "wasm32-wasi"
        "wasm64-wasi"
        "x86_64-linux"
      ];
      */
    };
  in
  {
    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#simple
    darwinConfigurations."MacBookPro" = nix-darwin.lib.darwinSystem {
      modules = [ configuration ];
    };

    # Expose the package set, including overlays, for convenience.
    darwinPackages = self.darwinConfigurations."MacBookPro".pkgs;
  };
}
